import "./utils/env"
import Client from './client'
import Discord from 'discord.js'
import { Player } from './player'
import fs from 'fs'
import path from 'path'
import Command from './commands/command'
import logger from './utils/logger'

const { PREFIX, TOKEN } = process.env

if (!PREFIX || !TOKEN) {
    console.error("You must defined prefix and token in .env file")
    process.exit(0)
}

logger.verbose("Loading commands")
const commandsDir = path.join(__dirname, "commands")

const client = new Client();
client.commands = new Discord.Collection<string, Command>()

const commandFiles = fs.readdirSync(commandsDir).filter(file => file !== "command.ts" && !file.startsWith("_") && (file.endsWith(".js") || file.endsWith(".ts")))

commandFiles.forEach(file => {
    let cmd = require(path.join(commandsDir, file)).default
    let cmdName = path.basename(file, path.extname(file))
    client.commands.set(cmdName, new cmd())
})

logger.verbose("Initializing player")
const player = Player(client)

client.once("ready", () => console.log("Ready !"))
client.once("reconnecting", () => console.log("Reconnecting !"))
client.once("disconnect", () => console.log("Disconnect !"))

client.on("messageCreate", async message => {

    if (message.author.bot || !message.guild || !client.application) return;
    if (!client.application.owner) {
        await client.application.fetch()
        if (!client.application.owner) return
    }

    if (message.content === `${PREFIX}deploy` && message.author.id === client.application.owner.id) {
        try {
            let commandsData = client.commands.map(cmd => cmd.commandData)
            await message.guild.commands.set(commandsData)
            logger.info("Commands deployed")
            message.reply("Deployed !")
        }
        catch (err) {
            logger.error("Could not deploy commands! Make sure the bot has the application.commands permission!")
            message.reply("Could not deploy commands! Make sure the bot has the application.commands permission!")
            console.log(err)
        }
    }
})

client.on("interactionCreate", async (interaction: Discord.Interaction) => {
    let commandInteraction = interaction as Discord.CommandInteraction
    let command = client.commands.get(commandInteraction.commandName.toLowerCase())

    if (!command) return

    logger.verbose(`Running command ${command.commandData.name}`)

    try {
        await command.execute(interaction, player)
        logger.verbose(`Command completed successfully`)
    }
    catch (err) {
        console.error(err)
        commandInteraction.followUp({
            content: 'There was an error trying to execute that command!'
        })
    }
})

logger.info(`Login du client`)
client.login(TOKEN)

process.on('uncaughtException', function (err) {
    logger.error('Caught exception: ' + err);
});