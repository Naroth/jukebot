import { Client as DiscordClient, ApplicationCommandData, Collection, Intents } from 'discord.js'
import Command from './commands/command'

export default class Client extends DiscordClient {

    commands: Collection<string, Command>

    // ApplicationCommandData[]
    queue: Map<any, any>
    config?: any

    constructor(config?: any) {
        super({
            intents: [
                Intents.FLAGS.GUILD_VOICE_STATES,
                Intents.FLAGS.GUILD_MESSAGES,
                Intents.FLAGS.GUILDS
            ]
        })

        this.commands = new Collection()

        this.queue = new Map()

        this.config = config
    }
}