import { Player } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Stop extends Command {

    constructor() {

        super({
            name: 'stop',
            description: 'Stop all songs and clear queue !',
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {

            logger.info("User want to stop Jukebot")

            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queue = await this.getQueue(interaction, player)
            if (queue.playing) {
                queue.destroy()
                interaction.followUp({ content: "🛑 | Stopped the player !" })
            }
            else {
                interaction.followUp({ content: '❌ | No music is being played !' });
            }
        }
        catch (err: any) {
            logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}