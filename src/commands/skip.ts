import { Player } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Skip extends Command {

    constructor() {

        super({
            name: 'skip',
            description: 'Skip to next song !',
            options: [
                {
                    name: 'position',
                    type: 4, // 'STRING' Type
                    description: 'Skip to song at position',
                    required: false,
                }
            ]
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {
            
            logger.info("User want to skip song")

            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queue = await this.getQueue(interaction, player)

            let position = interaction.options.get('position')

            if (queue.playing) {

                let currentTrack = queue.current

                if (!position) {
                    let success = queue.skip()
                    interaction.followUp({ content: success ? `✅ | Skipped **${currentTrack}** !` : '❌ | Something went wrong !' })
                }
                else {
                    let pos = parseInt(position.value as string)

                    if (pos < 1 || pos > queue.tracks.length) throw new Error(`There is no song at position ${pos}`)

                    let realPos = pos - 1
                    let targetTrack = queue.tracks[realPos]

                    queue.jump(targetTrack)
                    interaction.followUp({ content: `✅ | Jump to track ${targetTrack.title} ( ${targetTrack.author}) !` })
                }
            }
            else {
                interaction.followUp({ content: '❌ | No music is being played !' });
            }
        }
        catch (err: any) {
             logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}