import { Player } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Resume extends Command {

    constructor() {

        super({
            name: 'resume',
            description: 'Resume current song !',
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {

            logger.info("User request to resume song")
            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queue = await this.getQueue(interaction, player)
            if (queue.playing) {
                let success = queue.setPaused(false)
                interaction.followUp({ content: success ? '▶ | Resumed !' : '❌ | Something went wrong !', })
            }
            else {
                interaction.followUp({ content: '❌ | No music is being played !' });
            }
        }
        catch (err: any) {
            logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}