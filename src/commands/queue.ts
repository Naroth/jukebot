import { Player } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Queue extends Command {

    constructor() {

        super({
            name: 'queue',
            description: 'List songs in queue !',
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {

            logger.info("User list tracks in queue")

            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queue = await this.getQueue(interaction, player)

            if (queue.tracks.length > 0 || queue.playing) {
                interaction.followUp({
                    embeds: [{
                        title: 'Songs in queue : ',
                        description: `
                        🎶 | **${queue.current.title}** \nAuthor : ${queue.current.author}\n${queue.current.url}\n
                        ${queue.tracks.map((track, index) => `${index + 1} : **${track.title}** \nAuthor : ${track.author}\n${track.url}`).join("\n\n")}
                        `
                    }]
                })
            }
            else {
                interaction.followUp({ content: '❌ | Queue is empty !' });
            }

        }
        catch (err: any) {
            logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}