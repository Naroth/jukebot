import { Player, QueryType } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Queue extends Command {

    constructor() {

        super({
            name: 'remove',
            description: 'Remove a song from queue !',
            options: [
                {
                    name: 'position',
                    description: 'Position of the song to remove',
                    type: 4,
                    required: true
                }
            ]
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {

            logger.info("User remove track from queue")

            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queue = await this.getQueue(interaction, player)
            let position = interaction.options.get('position')

            if (!position)
                return void interaction.followUp({ content: 'Parameter query is missing' });

            if (queue.tracks.length > 0 || queue.playing) {
                let pos = parseInt(position.value as string)

                if (pos < 1 || pos > queue.tracks.length) throw new Error(`There is no song at position ${pos}`)

                let realPos = pos - 1
                let targetTrack = queue.tracks[realPos]

                queue.remove(targetTrack)
                interaction.followUp({ content: `✅ | Removed track ${targetTrack.title} ( ${targetTrack.author}) !` })
            }
            else {
                interaction.followUp({ content: '❌ | Queue is empty !' });
            }

        }
        catch (err: any) {
            logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}