import { Player, QueueRepeatMode } from "discord-player"
import { ApplicationCommandData, CommandInteraction, GuildMember } from "discord.js"
import logger from "../utils/logger"

export default abstract class Command {

    commandData: ApplicationCommandData

    constructor(commandData: ApplicationCommandData) {
        this.commandData = commandData
    }

    abstract execute: (interaction: any, player: any) => void

    botBasicCheck = (interaction: CommandInteraction) => {

        if (!(interaction.member instanceof GuildMember) || !interaction.member.voice.channel) {
            logger.error("User is not in a voide channel")
            interaction.reply({ content: 'You are not in a voice channel !', ephemeral: true });
            return false
        }

        if (!interaction.guild || !interaction.guild.me) {
            logger.error("User not found")
            interaction.reply({ content: 'I don\'t know who you are !', ephemeral: true })
            return false
        }
        
        if (interaction.guild.me.voice.channelId && interaction.member.voice.channelId !== interaction.guild.me.voice.channelId) {
            logger.error("User and bot are not ine the same channel")
            interaction.reply({ content: 'You are not in my voice channel !', ephemeral: true });
            return false
        }

        return true
    }

    getQueue = async (interaction: CommandInteraction, player: Player) => {


        try {
            logger.verbose("Retrieving queue")
            let queue = player.getQueue(interaction.guildId!)

            if (queue) return queue

            logger.verbose("Queue not found. Creating a new one")
            queue = await player.createQueue(interaction.guild!, {
                metadata: interaction.channel,
                leaveOnEmpty: false,
                leaveOnEnd: false,
                leaveOnStop: false,
                autoSelfDeaf: false,
                leaveOnEmptyCooldown: 300000
            });

            logger.verbose("Set queue mode to repeat all")
            // queue.setRepeatMode(QueueRepeatMode.QUEUE)

            try {

                if (!queue.connection) {
                    logger.verbose("Try to connect queue")
                    await queue.connect((interaction.member as GuildMember).voice.channelId!);
                }

                return queue
            }
            catch
            {
                void player.deleteQueue(interaction.guildId!);
                logger.error('Could not join your voice channel!')
                throw new Error('Could not join your voice channel!')
            }

        }
        catch (err) {
            logger.error("Unable to retrieve queue")
            throw new Error("Unable to retrieve queue")
        }
    }
}
