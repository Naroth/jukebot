import { Player, QueryType } from "discord-player";
import { CommandInteraction } from "discord.js";
import logger from "../utils/logger";
import Command from "./command";

export default class Play extends Command {

    constructor() {

        super({
            name: 'play',
            description: 'Play a song in your channel!',
            options: [
                {
                    name: 'query',
                    type: 3, // 'STRING' Type
                    description: 'The song you want to play',
                    required: true,
                },
                {
                    name: 'after',
                    type: 5,
                    description: 'Play this song right after current song',
                    required: false
                }
            ]
        })
    }

    execute = async (interaction: CommandInteraction, player: Player) => {

        try {

            logger.info("User add track to queue")

            if (!this.botBasicCheck(interaction)) return

            await interaction.deferReply()

            let queryParams = interaction.options.get('query')
            let afterParams = interaction.options.get('after')

            if (!queryParams)
                return void interaction.followUp({ content: 'Parameter query is missing' });

            let queue = await this.getQueue(interaction, player)
            let query = queryParams.value as string

            let queries = query.split(/\s+/g)

            await interaction.followUp({ content: `⏱ | Loading your ${queries.length > 1 ? "tracks" : "track"}...` });

            for await (let url of queries) {

                logger.info(`Looking for track ${url}`)
                let searchResult = await player.search(url, { requestedBy: interaction.user, searchEngine: QueryType.AUTO })

                if (afterParams && afterParams.value) {
                    logger.info(`Add track at first position ${url}`)
                    searchResult.playlist ?
                        searchResult.tracks.reverse().forEach(track => queue.insert(track, 0))
                        : queue.insert(searchResult.tracks[0], 0)
                }
                else {
                    logger.info(`Append track to queue ${url}`)
                    searchResult.playlist ? queue.addTracks(searchResult.tracks) : queue.addTrack(searchResult.tracks[0])
                }
            }

            if (!queue.playing) {
                logger.info("Play first song")
                queue.play()
            }

        }
        catch (err: any) {
            logger.error(err.message);
            interaction.followUp({ content: 'There was an error trying to execute that command: ' + err.message });
        }
    }
}