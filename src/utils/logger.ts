import winston from 'winston'
import "winston-daily-rotate-file"

const IS_DEV = Boolean(process.env.TS_NODE_DEV)

const customFormat = winston.format.printf(({ level, message, timestamp }) => {
    return `[${timestamp}] ${level} - ${message}`
})

const transports: winston.transport[] = []

transports.push(new winston.transports.Console)

if (!IS_DEV) {
    transports.push(new winston.transports.DailyRotateFile({
        filename: 'jukebot.log',
        dirname: './logs',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d'
    }))
}

const logger = winston.createLogger({
    level: process.env.LOG_LEVEL || 'info',
    transports: transports,
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        customFormat
    )
})


export default logger
