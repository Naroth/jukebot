# Jukebot

Un bot discord permettant de jouer de la musique
Ce projet est largement inspiré par [discord-bot](https://github.com/TannerGabriel/discord-bot)

## Sommaire
* [Getting started](#getting-started)
* [Permissions requises](#permissions-requises)
* [Niveaux de log](#niveaux-de-log)
* [Deployer les commandes](#deployer-les-commandes)
* [License](#license)

## Getting started

## Prérequis

- [Node](https://nodejs.org/en/) - Version 16 ou supérieur
- [Yarn](https://yarnpkg.com/)
- [NPM](https://www.npmjs.com/)
- [FFMPEG](https://www.ffmpeg.org/)

### Installation

```bash
# Clone le repository
git clone https://gitlab.com/Naroth/jukebot

# Ce déplacer dans le répertoire
cd jukebot/

# Installer les dépendances
yarn install
```

## Permissions requises

**Important:** Le bot doit avoir le scope d'application `applications.commands`. Il peut être trouvé dans la partie `OAuth2` du [Discord developer portal](https://discord.com/developers/applications/)

### Configuration

Renommer le fichier .env.example en .env et ajouter votre token discord API 

### Transpiler le typescript

```bash
yarn run build
```

### Démarrer l'application

```bash
cd dist
node index.js
```

## Niveaux de log

Jukebot utilise [Winston](https://github.com/winstonjs/winston) comme système de log  
Il est possible de changer le niveau de log de Jukebot dans le .env  
[Niveaux de log](https://github.com/winstonjs/winston#logging-levels) de Winston

## Deployer les commandes

Le bot utilisant des commandes slash avant de pouvoir vous en servir vous devez les ajouter au serveur. Vous pouvez utiliser la commande `!deploy` pour le faire  
Une fois le deploiement fait vous pouvez tapper / dans Discord pour lister les commandes disponibles

## License

Le projet est sous [Licence MIT](LICENCE) 